from chalice import Chalice, AuthResponse
from chalicelib import auth
import boto3
from boto3.dynamodb.types import Binary
import os
import json
import getpass
import hashlib
import hmac

app = Chalice(app_name='owstest')
app.debug = True
s3_resource = boto3.resource('s3')
_USER_DB = None
table_name = 'users'

@app.route('/')
def index():
    return {'hello': 'world'}


@app.route('/login', methods=['POST'])
def login():
    body = app.current_request.json_body
    record = get_users_db().get_item(
        Key={'username': body['username']})['Item']
    jwt_token = auth.get_jwt_token(
        body['username'], body['password'], record)
    return {'token': jwt_token.decode('utf-8')}


@app.authorizer()
def jwt_auth(auth_request):
    token = auth_request.token
    decoded = auth.decode_jwt_token(token)
    return AuthResponse(routes=['*'], principal_id=decoded['sub'])


# def get_table_name(stage):
#     # We might want to user the chalice modules to
#     # load the config.  For now we'll just load it directly.
#     with open(os.path.join('.chalice', 'config.json')) as f:
#         data = json.load(f)
#     return data['stages'][stage]['environment_variables']['USERS_TABLE_NAME']


@app.route('/create_user/{stage}/{username}/{password}', authorizer=jwt_auth)
def create_user(stage, username, password):
    # table_name = get_table_name(stage)
    table = boto3.resource('dynamodb').Table(table_name)
    password_fields = encode_password(password)
    item = {
        'username': username,
        'hash': password_fields['hash'],
        'salt': Binary(password_fields['salt']),
        'rounds': password_fields['rounds'],
        'hashed': Binary(password_fields['hashed']),
    }
    table.put_item(Item=item)


@app.route('/delete_user/{stage}/{username}', authorizer=jwt_auth)
def delete_user(stage, username):
    # table_name = get_table_name(stage)
    table = boto3.resource('dynamodb').Table(table_name)
    table.delete_item(TableName=table_name, Key={'username': username})


def encode_password(password, salt=None):
    if salt is None:
        salt = os.urandom(16)
    rounds = 100000
    hashed = hashlib.pbkdf2_hmac('sha256', bytes(password, 'utf-8'),
                                 salt, rounds)
    return {
        'hash': 'sha256',
        'salt': salt,
        'rounds': rounds,
        'hashed': hashed,
    }


@app.route('/list_users/{stage}', authorizer=jwt_auth)
def list_users(stage):
    # table_name = get_table_name(stage)
    table = boto3.resource('dynamodb').Table(table_name)
    usernames = []
    for item in table.scan()['Items']:
        # print(type(item['salt']))
        # b = item['salt']
        # print(b.value)
        usernames.append(item['username'])
    return usernames


def get_users_db():
    global _USER_DB
    if _USER_DB is None:
        _USER_DB = boto3.resource('dynamodb').Table(
            os.environ['USERS_TABLE_NAME'])
    return _USER_DB


@app.route('/create_bucket/{bucket_name}', authorizer=jwt_auth)
def create_bucket(bucket_name):
    print(bucket_name)
    s3_resource.create_bucket(Bucket=bucket_name,
                              CreateBucketConfiguration={
                                  'LocationConstraint': 'eu-central-1'})


@app.route('/delete_all_objects/{bucket_name}', authorizer=jwt_auth)
def delete_all_objects(bucket_name):
    res = []
    bucket = s3_resource.Bucket(bucket_name)
    for obj_version in bucket.object_versions.all():
        res.append({'Key': obj_version.object_key,
                    'VersionId': obj_version.id})
        print(res)
        bucket.delete_objects(Delete={'Objects': res})


@app.route('/delete_bucket/{bucket_name}', authorizer=jwt_auth)
def delete_bucket(bucket_name):
    s3_resource.Bucket(bucket_name).delete()
