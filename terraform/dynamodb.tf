resource "aws_dynamodb_table_item" "users" {
  table_name = "users"
  hash_key   = "${aws_dynamodb_table.users.hash_key}"

  item = <<ITEM
{
  "hash": {"S": "sha256"},
  "hashed": {"B": "G0doZP2teFQORyRsLIrcJxzUdVWlj1QVmEb8OOir6xE="},
  "rounds": {"N": "100000"},
  "salt": {"B": "8bff23dz7+H4Pe3Z2TfTlw=="},
  "username": {"S": "default"}
}
ITEM
}


resource "aws_dynamodb_table" "users" {
  name           = "users"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "username"

  attribute {
    name = "username"
    type = "S"
  }
}

resource "aws_iam_role_policy" "default_dynamodb" {
  name = "default-dynamodbRights"
  role = "owstest-dev"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "dynamodb:ListTables",
                "dynamodb:DeleteItem",
                "dynamodb:RestoreTableToPointInTime",
                "dynamodb:PurchaseReservedCapacityOfferings",
                "dynamodb:ListTagsOfResource",
                "dynamodb:UpdateGlobalTable",
                "dynamodb:CreateBackup",
                "dynamodb:DeleteTable",
                "dynamodb:UpdateContinuousBackups",
                "dynamodb:DescribeReservedCapacityOfferings",
                "dynamodb:DescribeTable",
                "dynamodb:GetItem",
                "dynamodb:DescribeContinuousBackups",
                "dynamodb:CreateGlobalTable",
                "dynamodb:DescribeLimits",
                "dynamodb:BatchGetItem",
                "dynamodb:UpdateTimeToLive",
                "dynamodb:BatchWriteItem",
                "dynamodb:ConditionCheckItem",
                "dynamodb:PutItem",
                "dynamodb:ListBackups",
                "dynamodb:Scan",
                "dynamodb:Query",
                "dynamodb:DescribeStream",
                "dynamodb:UpdateItem",
                "dynamodb:DescribeTimeToLive",
                "dynamodb:ListStreams",
                "dynamodb:CreateTable",
                "dynamodb:UpdateGlobalTableSettings",
                "dynamodb:DescribeGlobalTableSettings",
                "dynamodb:ListGlobalTables",
                "dynamodb:GetShardIterator",
                "dynamodb:DescribeGlobalTable",
                "dynamodb:DescribeReservedCapacity",
                "dynamodb:RestoreTableFromBackup",
                "dynamodb:DescribeBackup",
                "dynamodb:DeleteBackup",
                "dynamodb:UpdateTable",
                "dynamodb:GetRecords"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}
