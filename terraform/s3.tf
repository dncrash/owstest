resource "aws_iam_role_policy" "default_s3" {
  name = "default-s3"
  role = "owstest-dev"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "*"
        }
    ]
}
EOF
}
